import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ProductModel} from "../../models/products.model";
import {KidsProductsCrudService} from "../../services/kids-products-crud.service";

@Component({
  selector: 'app-kids-products-crud-form',
  templateUrl: './kids-products-crud-form.component.html',
  styleUrls: ['./kids-products-crud-form.component.css']
})
export class KidsProductsCrudFormComponent implements OnInit {

  kidsProducts: ProductModel;
  //@ts-ignore
  kidsProductsForm: FormGroup ;
  constructor(private router: Router,
              private fb: FormBuilder,
              private productCrudSvc: KidsProductsCrudService) {
    const navigation = this.router.getCurrentNavigation();
    //@ts-ignore
    this.kidsProducts = navigation!.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if (typeof this.kidsProducts === 'undefined'){
      this.router?.navigate(['new-of-kids-products']);
    }else{
      // @ts-ignore
      this.kidsProductsForm.patchValue(this.kidsProducts);
    }
  }

  private initForm(): void {
    this.kidsProductsForm = this.fb.group({
      title: ['', [Validators.required]],
      desc: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ratings: ['', [Validators.required]]
    });
  }

  isValidField(field: string): string{
    const validatedField = this.kidsProductsForm.get(field);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  onSave() {
    console.log('Saved', this.kidsProductsForm.value);
    if (this.kidsProductsForm.valid){
      const kidsProducts = this.kidsProductsForm.value;
      const kidsProductsId = this.kidsProducts?.id || null;
      //@ts-ignore
      this.productCrudSvc?.onSaveKidsProducts(kidsProducts, kidsProductsId);
      this.kidsProductsForm.reset();
    }
  }

  onGoBackToList(): void {
    this.router?.navigate(['list-of-kids-products']);
  }

}


/*
kidsProducts: ProductModel;
  //@ts-ignore
  kidsProductsForm: FormGroup ;
  constructor(private router: Router,
              private fb: FormBuilder) {
    const navigation = this.router!.getCurrentNavigation();
    // @ts-ignore
    this.kidsProducts = navigation!.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if (typeof this.kidsProducts == 'undefined'){
      this.router?.navigate(['new-of-kids-products']);
    }else{
      // @ts-ignore
      this.kidsProductsForm.patchValue(this.kidsProducts);
    }
  }

  private initForm(): void {
    this.kidsProductsForm = this.fb.group({
      title: ['', [Validators.required]],
      desc: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ratings: ['', [Validators.required]]
    })
  }

  onSave() {
    console.log('Saved', this.kidsProductsForm?.value);
    if (this.kidsProductsForm.valid){
      const kidsProducts = this.kidsProductsForm.value;
      const kidsProductsId = this.kidsProducts?.id || null;
      //@ts-ignore
      this.productCrudSvc.onSaveProducts(kidsProducts, kidsProductsId);
      this.kidsProductsForm.reset();
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-kids-products']);
  }
 */
