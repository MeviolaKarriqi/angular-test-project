import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { FirebaseService } from './firebase.service';
import {AdminService} from "./admin.service";
import {map, take, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthAdminGuard implements CanActivate {
  constructor(public backend: FirebaseService, public router: Router,
              private adminSvc: AdminService){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
    {
      return this.adminSvc.admin$.pipe(
        take(1),
        map( (admin) => admin && this.adminSvc.isAdmin(admin)),
          tap(canEdit => {
            if(!canEdit){
              window.alert('Access denied.');
            }
          })
      )
  }

}
