import { Observable, pipe } from 'rxjs';
import { ProductModel } from '../models/products.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, first, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {
  constructor(private db: AngularFirestore) { }

  getMockProducts(): Observable<ProductModel []>{
    return this.db.collection('products').snapshotChanges()
    .pipe(map(snaps =>{
      return snaps.map( snap =>{
        return<ProductModel>{
          id: snap.payload.doc.id,
          //@ts-ignore
          ...snap.payload.doc.data()
        };
      });
    }));
  }

  getMenProducts(): Observable<ProductModel []>{
    return this.db.collection('menProducts').snapshotChanges()
    .pipe(map(snaps =>{
      return snaps.map(snap =>{
        return<ProductModel>{
          id: snap.payload.doc.id,
          //@ts-ignore
          ...snap.payload.doc.data()
        }
      })
    }))
  }

  getKidsProducts(): Observable<ProductModel[]>{
    return this.db.collection('kidsProducts').snapshotChanges()
    .pipe(map(snaps => {
      return snaps.map(snap =>{
        return<ProductModel>{
          id: snap.payload.doc.id,
          //@ts-ignore
          ...snap.payload.doc.data()
        }
      })
    }))
  }
}
