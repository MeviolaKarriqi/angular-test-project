
import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ProductDataService } from 'src/app/services/product-data.service';
import {ShoppingCardService} from "../../services/shopping-card.service";
import {combineLatest, Subject} from "rxjs";
import {AngularFireAuth} from "@angular/fire/auth";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  //@ts-ignore
  product;
  //@ts-ignore
  searchTerm: string;
  startAt = new Subject();
  endAt = new Subject();
  startObs = this.startAt.asObservable();
  endObs = this.endAt.asObservable();


  @Input() products: any[] =[];
  constructor( private shoppingCard: ShoppingCardService,
    private fs: AngularFirestore,
    private prodService: ProductDataService,
               public afAuth: AngularFireAuth,) { }

  ngOnInit(): void {
    this.fs.collection('products').valueChanges().subscribe(data => console.log(data));
    this.getProducts();
    combineLatest(this.startObs, this.endObs).subscribe( (value) => {
      this.firequery( value[0] , value[1]).subscribe( (product) => {
        this.product = product;
      })
    })
  }

  //@ts-ignore
  search($event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q + '\uf8ff');
  }
  //@ts-ignore
  firequery(start, end){
    return this.fs.collection('products', ref => ref.limit(4).orderBy('title').startAt(start).endAt(end)).valueChanges();
  }




  //@ts-ignore
  addToCard(p){
    this.shoppingCard.addProduct(p)
  }

  getProducts(){
    this.prodService.getMockProducts().subscribe(respond =>{
      this.products = respond
    })
  }
  addToCardd(){
    console.log('added to card');
  }
}
