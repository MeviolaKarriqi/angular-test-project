// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCLm6LtJJiBdQAtfJPcyGBh2kA20fNx7M8",
    authDomain: "dumbo-7c585.firebaseapp.com",
    projectId: "dumbo-7c585",
    storageBucket: "dumbo-7c585.appspot.com",
    messagingSenderId: "744895500312",
    appId: "1:744895500312:web:d0da973f072c68a1c4736d",
    measurementId: "G-Q538G019XJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
