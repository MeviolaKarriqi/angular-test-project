import { Component } from '@angular/core';

@Component({
  selector: 'app-new-of-products',
  template: `<app-products-crud-form></app-products-crud-form>`
})
export class NewOfProductsComponent { }
