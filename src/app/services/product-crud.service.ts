import { Injectable } from '@angular/core';
import {async, Observable} from "rxjs";
import {ProductModel} from "../models/products.model";
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/firestore";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductCrudService {
  //@ts-ignore
  products: Observable<ProductModel[]>;
  //@ts-ignore
  private productsCollection: AngularFirestoreCollection<ProductModel>;
  constructor(private readonly afs: AngularFirestore) {
    this.productsCollection = afs.collection<ProductModel>('products');
    this.getProducts();
  }
  onDeleteProduct(productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const result = await this.productsCollection.doc(productId).delete();
        resolve( result);
      }catch (err){
        reject(err.message)
      }
    });
  }
  onSaveProducts(product: ProductModel, productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const id = productId || this.afs.createId();
        //@ts-ignore
        const data = {id, ...product};
        const result = await this.productsCollection.doc(id).set(data);
        resolve(result);
      }catch (err) {
        reject(err.message);
      }
    });
  }
  private getProducts(): void{
    this.products = this.productsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as ProductModel))
    );
  }
}
