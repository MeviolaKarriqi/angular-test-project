import { ShoppingCardService } from '../../services/shopping-card.service';
import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-checkout-products',
  templateUrl: './checkout-products.component.html',
  styleUrls: ['./checkout-products.component.css']
})
export class CheckoutProductsComponent implements OnInit {
  @Input() checkout_products: any[] = [];
  @Output() deleteEvent : EventEmitter<any> = new EventEmitter();
  constructor(public shoppingCard: ShoppingCardService) { }

  ngOnInit(): void {
    console.log('products', this.checkout_products)

  }

  //@ts-ignore
  removeItem(product){
    this.shoppingCard.removerItem(product);
    this.deleteEvent.emit(product);
  }
}
