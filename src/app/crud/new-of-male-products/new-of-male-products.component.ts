import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-of-male-products',
  template: `<app-male-products-crud-form></app-male-products-crud-form>`
})
export class NewOfMaleProductsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
