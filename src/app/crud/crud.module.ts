import { NgModule } from '@angular/core';
import { ListOfProductsComponent } from './list-of-products/list-of-products.component';
import { NewOfProductsComponent } from './new-of-products/new-of-products.component';
import { DetailsOfProductsComponent } from './details-of-products/details-of-products.component';
import { EditProductsComponent } from './edit-products/edit-products.component';
import { EditMaleProductsComponent } from './edit-male-products/edit-male-products.component';
import { ListOfMaleProductsComponent } from './list-of-male-products/list-of-male-products.component';
import { NewOfMaleProductsComponent } from './new-of-male-products/new-of-male-products.component';
import { DetailsOfManProductsComponent } from './details-of-man-products/details-of-man-products.component';
import { DetailsOfKidsProductsComponent } from './details-of-kids-products/details-of-kids-products.component';
import { EditKidsProductsComponent } from './edit-kids-products/edit-kids-products.component';
import { ListOfKidsProductsComponent } from './list-of-kids-products/list-of-kids-products.component';
import { NewOfKidsProductsComponent } from './new-of-kids-products/new-of-kids-products.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "../app-routing.module";
import {MaterialModule} from "../material/material.module";
import { ProductsCrudFormComponent } from './products-crud-form/products-crud-form.component';
import { MaleProductsCrudFormComponent } from './male-products-crud-form/male-products-crud-form.component';
import { KidsProductsCrudFormComponent } from './kids-products-crud-form/kids-products-crud-form.component';



@NgModule({
  declarations: [
    ListOfProductsComponent,
    NewOfProductsComponent,
    DetailsOfProductsComponent,
    EditProductsComponent,
    EditMaleProductsComponent,
    ListOfMaleProductsComponent,
    NewOfMaleProductsComponent,
    DetailsOfManProductsComponent,
    DetailsOfKidsProductsComponent,
    EditKidsProductsComponent,
    ListOfKidsProductsComponent,
    NewOfKidsProductsComponent,
    ProductsCrudFormComponent,
    MaleProductsCrudFormComponent,
    KidsProductsCrudFormComponent,
    EditMaleProductsComponent
  ],
  imports: [
    CommonModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    RouterModule,
    AppRoutingModule,
    NgbModule,
    MaterialModule
  ],
  exports: [

    ListOfProductsComponent,
    NewOfProductsComponent,
    DetailsOfProductsComponent,
    EditProductsComponent,
    EditMaleProductsComponent,
    ListOfMaleProductsComponent,
    NewOfMaleProductsComponent,
    DetailsOfManProductsComponent,
    DetailsOfKidsProductsComponent,
    EditKidsProductsComponent,
    ListOfKidsProductsComponent,
    NewOfKidsProductsComponent
  ]
})
export class CrudModule { }
