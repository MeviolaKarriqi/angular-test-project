import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {ProductCrudService} from "../../services/product-crud.service";

@Component({
  selector: 'app-list-of-products',
  templateUrl: './list-of-products.component.html',
  styleUrls: ['./list-of-products.component.css']
})
export class ListOfProductsComponent implements OnInit {
  products$ = this.productCrudSvc.products;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };


  constructor(private router : Router,
              private productCrudSvc: ProductCrudService) { }

  ngOnInit(): void {
  }

  onGoToEdit(product: any) {
    // @ts-ignore
    this.navigationExtras.state.value = product;
    this.router?.navigate(['edit-products'], this.navigationExtras);
  }

  onGoToSee(product: any) {
    //@ts-ignore
    this.navigationExtras.state.value = product;
    this.router?.navigate(['details-of-products'], this.navigationExtras);
  }

  async onGoToDelete(id: any): Promise<void> {
    try {
      await this.productCrudSvc.onDeleteProduct(id);
      alert('Deleted');
    }catch (err){
      console.log(err);
    }
  }
}
