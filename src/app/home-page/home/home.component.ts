import { ProductDataService } from '../../services/product-data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private prodService: ProductDataService) { }

  products: any[] = [];
  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(){
    this.prodService.getMockProducts().subscribe(respond =>{
      this.products = respond
    })
  }
  addToCard(){
    console.log('added to card');
  }
}
