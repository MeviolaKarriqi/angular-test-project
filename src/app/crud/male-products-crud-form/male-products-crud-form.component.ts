import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ProductModel} from "../../models/products.model";
import {MenProductsCrudService} from "../../services/men-products-crud.service";

@Component({
  selector: 'app-male-products-crud-form',
  templateUrl: './male-products-crud-form.component.html',
  styleUrls: ['./male-products-crud-form.component.css']
})
export class MaleProductsCrudFormComponent implements OnInit {

  maleProducts: ProductModel;
  //@ts-ignore
  maleProductsForm: FormGroup;
  constructor(private router: Router,
              private fb: FormBuilder,
              private productCrudSvc: MenProductsCrudService) {
    const navigation = this.router.getCurrentNavigation();
    //@ts-ignore
    this.maleProducts = navigation!.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if (typeof this.maleProducts === 'undefined'){
      this.router?.navigate(['new-of-male-products']);
    }else{
      // @ts-ignore
      this.maleProductsForm.patchValue(this.maleProducts.value);
    }
  }

  isValidField(field: string): string{
    const validatedField = this.maleProductsForm.get(field);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  private initForm(): void {
    this.maleProductsForm = this.fb.group({
      title: ['', [Validators.required]],
      desc: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ratings: ['', [Validators.required]]
    });
  }

  onSave() {
    console.log('Saved', this.maleProductsForm.value);
    if (this.maleProductsForm.valid){
      const maleProducts = this.maleProductsForm.value;
      const maleProductsId = this.maleProducts?.id || null;
      //@ts-ignore
      this.productCrudSvc?.onSaveManProducts(maleProducts, maleProductsId);
      this.maleProductsForm.reset();
    }
  }

  onGoBackToList(): void {
    this.router?.navigate(['list-of-male-products']);
  }
}




/*
maleProducts: ProductModel;
  //@ts-ignore
  maleProductsForm: FormGroup;
  constructor(private router: Router,
              private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();
    // @ts-ignore
    this.maleProducts = navigation?.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if (typeof this.maleProductsForm === 'undefined') {
      this.router?.navigate(['new-of-male-products']);
    }else{
      // @ts-ignore
      this.maleProductsForm.patchValue(this.maleProducts.value);
    }
  }

  private initForm(): void {
    this.maleProductsForm = this.fb.group({
      title: ['', [Validators.required]],
      desc: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ratings: ['', [Validators.required]]
    });
  }

  onSave() {
    console.log('Saved', this.maleProductsForm.value);
    if (this.maleProductsForm.valid){
      const maleProducts = this.maleProductsForm.value;
      const maleProductsId = this.maleProducts?.id || null;
      //@ts-ignore
      this.productCrudSvc.onSaveProducts(maleProducts, maleProductsId);
      this.maleProductsForm.reset();
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-male-products']);
  }
 */
