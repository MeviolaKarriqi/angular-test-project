import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import auth from 'firebase';
import { BehaviorSubject, Observable } from 'rxjs';
import { AdminsModel } from '../models/admins.model';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  //@ts-ignore
  // private isAdmin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>('');
  //@ts-ignore
  // loggedIn: false;

  // myAdmins: Observable<boolean> = this.isAdmin.asObservable();
  //private _eStoreColl: string = "admins";

  authState: any = null;

   constructor( public afAuth: AngularFireAuth, private afs: AngularFirestore) {
    //  this.afAuth.authState.subscribe( authState => {
    //    this.authState = authState;
    //  });
   }


   getAllAdmin(): Observable<AdminsModel[]>{
     return this.afs.collection('admins').valueChanges() as Observable<AdminsModel[]>;
   }
  /*
  isLoggedIn():boolean {
    return  this.loggedIn;
  }

  setLoggedIn(loggedIn: boolean): void {
    this.loggedIn = loggedIn;
  }
*/


//   getAdminPortal(coll?: string){
//     if (!coll || coll == "store") { coll = this._eStoreColl; }
//     let x;
//     return this.getDoc(coll,this.authState.uid);
//   }
//   getDoc(coll: string, docId: string) {
//     return this.afs.collection(coll).doc(docId).valueChanges();
//   }

//   logout() {
//     window.localStorage.removeItem("displayName");
//     window.localStorage.removeItem("email");
//     window.localStorage.removeItem("token");
//     return this.afAuth.signOut();
// }
}
