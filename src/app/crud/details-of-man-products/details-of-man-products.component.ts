import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {ProductModel} from "../../models/products.model";
import {MenProductsCrudService} from "../../services/men-products-crud.service";

@Component({
  selector: 'app-details-of-man-products',
  templateUrl: './details-of-man-products.component.html',
  styleUrls: ['./details-of-man-products.component.css']
})
export class DetailsOfManProductsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }
  maleProducts: ProductModel;
  constructor(private router: Router,
              private maleProductSvc: MenProductsCrudService) {
    const navigation = this.router.getCurrentNavigation();
    this.maleProducts = navigation?.extras?.state?.value;
  }

  ngOnInit(): void {
    if (typeof  this.maleProducts == 'undefined'){
      this.router?.navigate(['list-of-male-products']);
    }
  }

  onGoToEdit() {
    this.navigationExtras!.state!.value = this.maleProducts;
    this.router?.navigate(['edit-male-products'], this.navigationExtras);
  }

  async onDelete(): Promise<void> {
    try {
      //@ts-ignore
      await this.maleProductSvc.onDeleteMenProduct(this.maleProducts?.id);
      alert('Deleted');
      this.onGoBackToList();
    }catch (err){
      console.log(err);
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-male-products']);
  }
}


/*
navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }
  //@ts-ignore
  maleProducts = ProductModel;
  constructor(private router: Router,
              private maleProductSvc: MenProductsCrudService) {
    const navigation = this.router.getCurrentNavigation();
    this.maleProducts = navigation?.extras?.state?.value;
  }

  ngOnInit(): void {
    if (typeof this.maleProducts == 'undefined'){
      this.router?.navigate(['list-of-male-products']);
    }
  }

  onGoToEdit() {
    this.navigationExtras!.state!.values = this.maleProducts;
    this.router?.navigate(['edit-male-products'], this.navigationExtras);
  }

  async onDelete(): Promise<void> {
    try {
      // @ts-ignore
      await this.maleProductSvc.onDeleteMenProduct(this.maleProducts?.id);
      alert('Deleted');
    }catch (e) {
      console.log(e);
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-male-products']);
  }
 */
