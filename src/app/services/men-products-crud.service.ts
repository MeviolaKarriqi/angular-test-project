import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ProductModel} from "../models/products.model";
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/firestore";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MenProductsCrudService {
  //@ts-ignore
  maleProducts: Observable<ProductModel[]>;
  //@ts-ignore
  private maleProductsCollection: AngularFirestoreCollection<ProductModel>;
  constructor(private readonly afs: AngularFirestore) {
    this.maleProductsCollection = afs.collection<ProductModel>('menProducts');
    this.getManProducts();
  }
  onDeleteMenProduct(productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const result = await this.maleProductsCollection.doc(productId).delete();
        resolve( result);
      }catch (err){
        reject(err.message)
      }
    });
  }
  onSaveManProducts(product: ProductModel, productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const id = productId || this.afs.createId();
        //@ts-ignore
        const data = {id, ...product};
        const result = await this.maleProductsCollection.doc(id).set(data);
        resolve(result);
      }catch (err) {
        reject(err.message);
      }
    })
  }

  private getManProducts(): void{
    this.maleProducts = this.maleProductsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as ProductModel))
    );
  }
}
