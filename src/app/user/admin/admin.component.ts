import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import {AdminService} from "../../services/admin.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  //@ts-ignore
  adminLogInForm: FormGroup;
  //@ts-ignore
  firebaseErrorMessage: string;

  constructor( private adminService: AdminService
              , private fb: FormBuilder, private router: Router) {
                this.adminLogInForm = new FormGroup({
                  'email': new FormControl('', [Validators.required, Validators.email]),
                  'password': new FormControl('', Validators.required)
                });
                this.firebaseErrorMessage = '';
               }

  ngOnInit(): void {

  }
  async onLogin() {
    const { email, password } = this.adminLogInForm.value;
    try {
      const user = await this.adminService.adminLogin(email, password);
    } catch (error) {
      console.log(error);
    }
  }
}
