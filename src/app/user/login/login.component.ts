import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //@ts-ignore
  loginForm: FormGroup;
  //@ts-ignore
  firebaseErrorMessage: string;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, private afAuth: AngularFireAuth) {

    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required)
  });
  this.firebaseErrorMessage = '';
  }

  ngOnInit(): void {

  }

  loginUser(){
    if(this.loginForm.invalid){
      return;
    }
    this.authService.loginUser(this.loginForm.value.email, this.loginForm.value.password).then((result) => {
      if(result == null){
        console.log('logging in...');
        this.router.navigate(['/']);
      }else if(result.isValid == false){
        console.log('log in error', result);
        this.firebaseErrorMessage = result.message;
      }
    });
  }
}
