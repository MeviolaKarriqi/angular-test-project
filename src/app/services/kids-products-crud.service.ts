import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ProductModel} from "../models/products.model";
import {AngularFirestore, AngularFirestoreCollection} from "@angular/fire/firestore";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class KidsProductsCrudService {
  //@ts-ignore
  kidsProducts: Observable<ProductModel[]>;
  //@ts-ignore
  private kidsProductsCollection: AngularFirestoreCollection<ProductModel>;

  constructor(private readonly afs: AngularFirestore) {
    this.kidsProductsCollection = afs.collection<ProductModel>('kidsProducts');
    this.getKidsProducts();
  }
  onDeleteKidsProduct(productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const result = await this.kidsProductsCollection.doc(productId).delete();
        resolve( result);
      }catch (err){
        reject(err.message)
      }
    });
  }

  onSaveKidsProducts(product: ProductModel, productId: string): Promise<void>{
    return new Promise(async (resolve, reject) => {
      try{
        const id = productId || this.afs.createId();
        //@ts-ignore
        const data = {id, ...product};
        const result = await this.kidsProductsCollection.doc(id).set(data);
        resolve(result);
      }catch (err) {
        reject(err.message);
      }
    })
  }

  private getKidsProducts(): void{
    this.kidsProducts = this.kidsProductsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as ProductModel))
    );
  }
}
