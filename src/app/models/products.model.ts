export interface ProductModel{
  id?: string;
  title: string;
  desc: string;
  price: number;
  image: string;
  ratings: number;
}
