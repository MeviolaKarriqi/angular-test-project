import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ProductModel } from 'src/app/models/products.model';
import { ProductDataService } from 'src/app/services/product-data.service';
import { ShoppingCardService } from 'src/app/services/shopping-card.service';
import {combineLatest, Subject} from "rxjs";

@Component({
  selector: 'app-kids-products',
  templateUrl: './kids-products.component.html',
  styleUrls: ['./kids-products.component.css']
})
export class KidsProductsComponent implements OnInit {
  // @ts-ignore
  searchTerm: string;
  startAt = new Subject();
  endAt = new Subject();
  startObs = this.startAt.asObservable();
  endObs = this.endAt.asObservable();
  //@ts-ignore
  products;

  @Input() kidsProducts: ProductModel[] =[];
  constructor(private shoppingCard: ShoppingCardService,
    private fs: AngularFirestore,
    private prodService: ProductDataService) { }

  ngOnInit(): void {
    combineLatest(this.startObs, this.endObs).subscribe( (value) => {
      this.firequery( value[0] , value[1]).subscribe( (products) => {
        this.products = products;
      })
    })


    this.fs.collection('kidsProducts').valueChanges().subscribe(data => console.log(data));
    this.getKidsProducts();
  }


  //@ts-ignore
  addToCard(p){
    this.shoppingCard.addProduct(p)
  }

  getKidsProducts(){
    this.prodService.getKidsProducts().subscribe(respond =>{
      this.kidsProducts = respond
    })
  }
  addToCardd(){
    console.log('added to card');
  }

  //@ts-ignore
  search( $event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q + '\uf8ff');
  }
  //@ts-ignore
  firequery(start, end){
    return this.fs.collection('kidsProducts', ref => ref.limit(4).orderBy('title').startAt(start).endAt(end)).valueChanges();
  }

}
