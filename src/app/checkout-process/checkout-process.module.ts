import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CheckoutProductsComponent} from "./checkout-products/checkout-products.component";
import {CheckoutSubtotalComponent} from "./checkout-subtotal/checkout-subtotal.component";
import {CheckoutComponent} from "./checkout/checkout.component";
import {AllProductsModule} from "../all-products/all-products.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "../app-routing.module";
import {MaterialModule} from "../material/material.module";
import { PaypalComponent } from './paypal/paypal.component';



@NgModule({
  declarations: [
    CheckoutProductsComponent,
    CheckoutSubtotalComponent,
    CheckoutComponent,
    PaypalComponent,
  ],
  imports: [
    CommonModule,
    CommonModule,
    AllProductsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    RouterModule,
    AppRoutingModule,
    MaterialModule
  ],
  exports: [
    CheckoutProductsComponent,
    CheckoutSubtotalComponent,
    CheckoutComponent,
  ]
})
export class CheckoutProcessModule { }
