import { Component, OnInit } from '@angular/core';
import { ShoppingCardService } from 'src/app/services/shopping-card.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  items: any[] = [];
  constructor(public shoppingCard: ShoppingCardService ) { }

  ngOnInit(): void {
    this.getShoppingCard();
  }

  getShoppingCard(){
    this.items = this.shoppingCard.getShoppingCardItems();
  }
  //@ts-ignore
  deleteEventHandler(product){
    this.getShoppingCard();
  }
}
