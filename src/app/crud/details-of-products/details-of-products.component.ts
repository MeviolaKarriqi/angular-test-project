import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {ProductModel} from "../../models/products.model";
import {ProductCrudService} from "../../services/product-crud.service";

@Component({
  selector: 'app-details-of-products',
  templateUrl: './details-of-products.component.html',
  styleUrls: ['./details-of-products.component.css']
})
export class DetailsOfProductsComponent implements OnInit {

  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }
  products: ProductModel;
  constructor(private router: Router,
              private productsCrudSvc: ProductCrudService) {
    const navigation = this.router.getCurrentNavigation();
    this.products = navigation?.extras?.state?.value;
  }

  ngOnInit(): void {
    if (typeof  this.products == 'undefined'){
      this.router?.navigate(['list-of-products']);
    }
  }

  onGoToEdit() {
    this.navigationExtras!.state!.value = this.products;
    this.router?.navigate(['edit-products'], this.navigationExtras);
  }

  async onDelete(): Promise<void> {
    try {
      //@ts-ignore
      await this.productsCrudSvc.onDeleteProduct(this.products?.id);
      alert('Deleted');
      this.onGoBackToList();
    }catch (err){
      console.log(err);
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-products']);
  }
}
