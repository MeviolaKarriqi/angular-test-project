export type Roles = 'Admin';
export interface User{
  uid: string;
  email: string;
  password: string;
  //isAdmin: string;
  role?: Roles;
}
