import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home-page/home/home.component";
import {CheckoutComponent} from "./checkout-process/checkout/checkout.component";
import {LoginComponent} from "./user/login/login.component";
import {SignupComponent} from "./user/signup/signup.component";
import {ForgotPasswordComponent} from "./user/forgot-password/forgot-password.component";
import { ProductsComponent } from './all-products/products/products.component';
import { MenProductsComponent } from './all-products/men-products/men-products.component';
import { KidsProductsComponent } from './all-products/kids-products/kids-products.component';
import { AdminComponent } from './user/admin/admin.component';
import {DetailsOfProductsComponent} from "./crud/details-of-products/details-of-products.component";
import {DetailsOfManProductsComponent} from "./crud/details-of-man-products/details-of-man-products.component";
import {DetailsOfKidsProductsComponent} from "./crud/details-of-kids-products/details-of-kids-products.component";
import {EditProductsComponent} from "./crud/edit-products/edit-products.component";
import {EditMaleProductsComponent} from "./crud/edit-male-products/edit-male-products.component";
import {EditKidsProductsComponent} from "./crud/edit-kids-products/edit-kids-products.component";
import {ListOfProductsComponent} from "./crud/list-of-products/list-of-products.component";
import {ListOfMaleProductsComponent} from "./crud/list-of-male-products/list-of-male-products.component";
import {ListOfKidsProductsComponent} from "./crud/list-of-kids-products/list-of-kids-products.component";
import {NewOfProductsComponent} from "./crud/new-of-products/new-of-products.component";
import {NewOfMaleProductsComponent} from "./crud/new-of-male-products/new-of-male-products.component";
import {NewOfKidsProductsComponent} from "./crud/new-of-kids-products/new-of-kids-products.component";
import {PaypalComponent} from "./checkout-process/paypal/paypal.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'checkout', component: CheckoutComponent},
  {path: 'signup', component:SignupComponent},
  {path: 'forgot-password', component:ForgotPasswordComponent},
  {path: 'products', component:ProductsComponent},
  {path: 'menProducts', component:MenProductsComponent},
  {path: 'kidsProducts', component:KidsProductsComponent},
  {path: 'admin', component:AdminComponent},
  {path: 'details-of-products', component:DetailsOfProductsComponent},
  {path: 'details-of-man-products', component:DetailsOfManProductsComponent},
  {path: 'details-of-kids-products', component:DetailsOfKidsProductsComponent},
  {path: 'edit-products', component:EditProductsComponent},
  {path: 'edit-kids-products', component:EditKidsProductsComponent},
  {path: 'edit-male-products', component:EditMaleProductsComponent},
  {path: 'list-of-products', component:ListOfProductsComponent},
  {path: 'list-of-male-products', component:ListOfMaleProductsComponent},
  {path: 'list-of-kids-products', component:ListOfKidsProductsComponent},
  {path: 'new-of-products', component:NewOfProductsComponent},
  {path: 'new-of-male-products', component:NewOfMaleProductsComponent},
  {path: 'new-of-kids-products', component:NewOfKidsProductsComponent},
  {path: 'paypal', component: PaypalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
