import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ShoppingCardService } from 'src/app/services/shopping-card.service';
import { ProductModel } from '../../models/products.model';
import Swiper from 'swiper/bundle';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-products-slider',
  templateUrl: './products-slider.component.html',
  styleUrls: ['./products-slider.component.css']
})
export class ProductsSliderComponent implements OnInit {

  @Input() products: ProductModel[] =[];
  constructor( private shoppingCard: ShoppingCardService,
    private fs: AngularFirestore) { }

    //@ts-ignore
    @ViewChild('ngcarousel', { static: true }) ngCarousel: NgbCarousel;
  ngOnInit(): void {
    this.fs.collection('products').valueChanges().subscribe(data => console.log(data));
  }
  //@ts-ignore
  addToCard(p){
    this.shoppingCard.addProduct(p)
  }


  //@ts-ignore
  navigateToSlide(item) {
    this.ngCarousel.select(item);
    console.log(item)
  }

  // Move to previous slide
  getToPrev() {
    this.ngCarousel.prev();
  }

  // Move to next slide
  goToNext() {
    this.ngCarousel.next();
  }

  // Pause slide
  stopCarousel() {
    this.ngCarousel.pause();
  }

  // Restart carousel
  restartCarousel() {
    this.ngCarousel.cycle();
  }


  slideActivate(ngbSlideEvent: NgbSlideEvent) {
    console.log(ngbSlideEvent.source);
    console.log(ngbSlideEvent.paused);
    console.log(NgbSlideEventSource.INDICATOR);
    console.log(NgbSlideEventSource.ARROW_LEFT);
    console.log(NgbSlideEventSource.ARROW_RIGHT);
  }

}
