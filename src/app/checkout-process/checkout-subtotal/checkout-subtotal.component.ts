import { Component, OnInit } from '@angular/core';
import { ShoppingCardService } from 'src/app/services/shopping-card.service';

@Component({
  selector: 'app-checkout-subtotal',
  templateUrl: './checkout-subtotal.component.html',
  styleUrls: ['./checkout-subtotal.component.css']
})
export class CheckoutSubtotalComponent implements OnInit {

  constructor(public shoppingCard: ShoppingCardService) { }

  ngOnInit(): void {
  }
  onClickCheckout(){
    alert("Done");
  }

}
