import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //@ts-ignore
  userLoggedIn: boolean;

  constructor(private afAuth: AngularFireAuth, private router: Router) {
    this.userLoggedIn = false;
    this.afAuth.onAuthStateChanged((user)=>{
      if(user){
        this.userLoggedIn = true;
      }else{
        this.userLoggedIn = false;
      }
    });
  }

  loginUser(email: string, password: string):Promise<any> {
    return this.afAuth.signInWithEmailAndPassword(email, password).then(() => {
      console.log('AuthService: loginUser: success');
    })
    //@ts-ignore
    .catch(error =>{
      console.log('AuthService: login error...');
      console.log('error code: ' + error.code);
      console.log('error', error);
      if(error.code){
        return {isValid: false, message: error.message};
      }
    });
  }

  signupUser(user: any): Promise<any>{
    return this.afAuth.createUserWithEmailAndPassword(user.email, user.password).then((result) =>{
      let emailLower = user.email.toLowerCase();
      result.user?.sendEmailVerification();
    })
    //@ts-ignore
    .catch((error) =>{
      console.log('AuthService: signup error', error);
      if(error.code){
        return {isValid: false, message: error.message};
      }
    });
  }

  resetPassword(email:string): Promise<any>{
    return this.afAuth.sendPasswordResetEmail(email).then(() =>{
      console.log('AuthService: reset password success');
    }).catch(error =>{
      console.log('AuthService: reset password error...');
      console.log(error.code);
      console.log(error)
      if(error.code){
        return error;
      }
    })
  }

  loggedIn = false;

  setAdminLoggedIn(loggedIn:boolean): void{
    this.loggedIn = loggedIn;
  }
}
