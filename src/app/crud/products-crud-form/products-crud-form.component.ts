import { Component, OnInit } from '@angular/core';
import {ProductModel} from "../../models/products.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ProductCrudService} from "../../services/product-crud.service";

@Component({
  selector: 'app-products-crud-form',
  templateUrl: './products-crud-form.component.html',
  styleUrls: ['./products-crud-form.component.css']
})
export class ProductsCrudFormComponent implements OnInit {

  products: ProductModel;
  //@ts-ignore
  productsForm: FormGroup;
  constructor(private router: Router,
              private fb: FormBuilder,
              private productCrudSvc: ProductCrudService) {
    const navigation = this.router.getCurrentNavigation();
    //@ts-ignore
    this.products = navigation!.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {
    if (typeof this.products === 'undefined'){
      this.router?.navigate(['new-of-products']);
    }else{
      this.productsForm.patchValue(this.products);
    }
  }

  private initForm(): void {
    this.productsForm = this.fb.group({
      title: ['', [Validators.required]],
      desc: ['', [Validators.required]],
      price: ['', [Validators.required]],
      image: ['', [Validators.required]],
      ratings: ['', [Validators.required]]
    });
  }

  isValidField(field: string): string{
    const validatedField = this.productsForm.get(field);
    return (!validatedField?.valid && validatedField?.touched)
      ? 'is-invalid' : validatedField?.touched ? 'is-valid' : '';
  }

  onSave() {
    console.log('Saved', this.productsForm.value);
    if (this.productsForm.valid){
      const products = this.productsForm.value;
      const productsId = this.products?.id || null;
      //@ts-ignore
      this.productCrudSvc?.onSaveProducts(products, productsId);
      this.productsForm.reset();
    }
  }

  onGoBackToList(): void {
    this.router?.navigate(['list-of-products']);
  }

}
