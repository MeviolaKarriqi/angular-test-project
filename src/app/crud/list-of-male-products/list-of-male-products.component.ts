import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {MenProductsCrudService} from "../../services/men-products-crud.service";
import {ProductCrudService} from "../../services/product-crud.service";

@Component({
  selector: 'app-list-of-male-products',
  templateUrl: './list-of-male-products.component.html',
  styleUrls: ['./list-of-male-products.component.css']
})
export class ListOfMaleProductsComponent implements OnInit {
  maleProducts$ = this.maleProductSvc.maleProducts;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };


  constructor(private router : Router,
              private maleProductSvc: MenProductsCrudService) { }

  ngOnInit(): void {
  }

  onGoToEdit(product: any) {
    // @ts-ignore
    this.navigationExtras.state.value = product;
    this.router?.navigate(['edit-male-products'], this.navigationExtras);
  }

  onGoToSee(product: any) {
    //@ts-ignore
    this.navigationExtras.state.value = product;
    this.router?.navigate(['details-of-man-products'], this.navigationExtras);
  }

  async onGoToDelete(id: any): Promise<void> {
    try {
      await this.maleProductSvc.onDeleteMenProduct(id);
      alert('Deleted');
    }catch (err){
      console.log(err);
    }
  }
}


/*
maleProducts$ = this.maleProductSvc.maleProducts;
  navigationExtras: NavigationExtras = {
    state: {
      values: null
    }
  }
  constructor( private router: Router,
               private maleProductSvc: MenProductsCrudService) { }

  ngOnInit(): void {
  }

  onGoToEdit(product: any) {
    // @ts-ignore
    this.navigationExtras.state.values = product;
    this.router?.navigate(['edit-male-products'], this.navigationExtras);
  }

  onGoToSee(product: any) {
    // @ts-ignore
    this.navigationExtras.state.values = product;
    this.router?.navigate(['details-of-man-products'], this.navigationExtras);
  }

  onGoToDelete(id: any) {
    alert('Deleted');
  }
 */
