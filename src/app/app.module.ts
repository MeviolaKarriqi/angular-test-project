import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ProductDataService } from './services/product-data.service';
import {UserModule} from "./user/user.module";
import {HomePageModule} from "./home-page/home-page.module";
import {CheckoutProcessModule} from "./checkout-process/checkout-process.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from "./material/material.module";
import {CrudModule} from "./crud/crud.module";
import {AdminService} from "./services/admin.service";

@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgbModule,
    MaterialModule,

    UserModule,
    HomePageModule,
    CheckoutProcessModule,
    CrudModule

  ],
  exports:[
    NgbModule
  ],
  providers: [ProductDataService, AdminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
