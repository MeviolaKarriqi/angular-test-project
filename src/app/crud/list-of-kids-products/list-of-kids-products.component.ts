import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {KidsProductsCrudService} from "../../services/kids-products-crud.service";
import {ProductCrudService} from "../../services/product-crud.service";

@Component({
  selector: 'app-list-of-kids-products',
  templateUrl: './list-of-kids-products.component.html',
  styleUrls: ['./list-of-kids-products.component.css']
})
export class ListOfKidsProductsComponent implements OnInit {
  kidsProducts$ = this.productCrudSvc.kidsProducts;
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };
  constructor(private router : Router,
              private productCrudSvc: KidsProductsCrudService) { }

  ngOnInit(): void {
  }

  onGoToEdit(kidsProducts: any) {
    // @ts-ignore
    this.navigationExtras.state.value = kidsProducts;
    this.router?.navigate(['edit-kids-products'], this.navigationExtras);
  }

  onGoToSee(kidsProducts: any) {
    //@ts-ignore
    this.navigationExtras.state.value = kidsProducts;
    this.router?.navigate(['details-of-kids-products'], this.navigationExtras);
  }

  async onGoToDelete(id: any): Promise<void> {
    try {
      await this.productCrudSvc.onDeleteKidsProduct(id);
      alert('Deleted');
    }catch (err){
      console.log(err);
    }
  }
}


/*
kidsProducts$ = this.kidsProductsSvc.kidsProducts;
  navigationExtras: NavigationExtras = {
    state: {
      values: null
    }
  }
  constructor(private router: Router,
              private kidsProductsSvc: KidsProductsCrudService) { }

  ngOnInit(): void {
  }

  onGoToEdit(product: any) {
    // @ts-ignore
    this.navigationExtras.state.values = product;
    this.router?.navigate(['edit-kids-products'], this.navigationExtras);
  }

  onGoToSee(product: any) {
    // @ts-ignore
    this.navigationExtras.state.values = product;
    this.router?.navigate(['details-of-kids-products'], this.navigationExtras);
  }

  onGoToDelete(id: any) {
    alert('Deleted');
  }
 */
