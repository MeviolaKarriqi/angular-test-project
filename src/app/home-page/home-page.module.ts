import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from "./header/header.component";
import {HomeComponent} from "./home/home.component";
import {BannerComponent} from "./banner/banner.component";
import {FooterComponent} from "./footer/footer.component";
import {AllProductsModule} from "../all-products/all-products.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "../app-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from "../material/material.module";



@NgModule({
  declarations: [
    HeaderComponent,
    HomeComponent,
    BannerComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    AllProductsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    RouterModule,
    AppRoutingModule,
    NgbModule,
    MaterialModule
  ],
  exports: [
    HeaderComponent,
    HomeComponent,
    BannerComponent,
    FooterComponent,
  ]
})
export class HomePageModule { }
