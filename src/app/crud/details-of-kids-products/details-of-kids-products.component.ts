import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from "@angular/router";
import {ProductModel} from "../../models/products.model";
import {KidsProductsCrudService} from "../../services/kids-products-crud.service";

@Component({
  selector: 'app-details-of-kids-products',
  templateUrl: './details-of-kids-products.component.html',
  styleUrls: ['./details-of-kids-products.component.css']
})
export class DetailsOfKidsProductsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }
  kidsProducts: ProductModel;
  constructor(private router: Router,
              private productsCrudSvc: KidsProductsCrudService) {
    const navigation = this.router.getCurrentNavigation();
    this.kidsProducts = navigation?.extras?.state?.value;
  }

  ngOnInit(): void {
    if (typeof  this.kidsProducts == 'undefined'){
      this.router?.navigate(['list-of-kids-products']);
    }
  }

  onGoToEdit() {
    this.navigationExtras!.state!.value = this.kidsProducts;
    this.router?.navigate(['edit-kids-products'], this.navigationExtras);
  }

  async onDelete(): Promise<void> {
    try {
      //@ts-ignore
      await this.productsCrudSvc.onDeleteKidsProduct(this.kidsProducts?.id);
      alert('Deleted');
      this.onGoBackToList();
    }catch (err){
      console.log(err);
    }
  }

  onGoBackToList() {
    this.router?.navigate(['list-of-kids-products']);
  }
}


/*
navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  }
  kidsProducts = null;
  constructor(private router: Router) {
    const navigation = this.router.getCurrentNavigation();
    //@ts-ignore
    this.kidsProducts = navigation?.extras?.state;
  }

  ngOnInit(): void {
  }

  onGoToEdit() {
    // @ts-ignore
    this.navigationExtras.state.values = this.kidsProducts;
    this.router.navigate(['edit-kids-products'], this.navigationExtras);
  }

  onDelete() {
    alert('Deleted');
  }

  onGoBackToList() {
    this.router.navigate(['list-of-kids-products']);
  }
 */
