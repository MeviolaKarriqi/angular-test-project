
import {User} from "../models/admins.model";

export class RoleValidator{
  isAdmin(admin: User): boolean{
    // @ts-ignore
    return admin.role === 'ADMIN';
  }
}
