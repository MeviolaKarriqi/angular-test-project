import { ShoppingCardService } from '../../services/shopping-card.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {AdminService} from "../../services/admin.service";
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable, Subject, combineLatest} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  // @ts-ignore
  searchTerm: string;
  startAt = new Subject();
  endAt = new Subject();
  startObs = this.startAt.asObservable();
  endObs = this.endAt.asObservable();
  //@ts-ignore
  products;
  constructor(public shoppingCard : ShoppingCardService ,
              public afAuth: AngularFireAuth,
              public adminService: AdminService,
              private afs: AngularFirestore) { }

  ngOnInit(): void {

    combineLatest(this.startObs, this.endObs).subscribe( (value) => {
      this.firequery( value[0] , value[1]).subscribe( (products) => {
        this.products = products;
      })
    })
  }

  logout(): void {
    this.afAuth.signOut();
  }
  //@ts-ignore
  search( $event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q + '\uf8ff');
  }
  //@ts-ignore
  firequery(start, end){
    return this.afs.collection('products', ref => ref.limit(4).orderBy('title').startAt(start).endAt(end)).valueChanges();
  }
}
