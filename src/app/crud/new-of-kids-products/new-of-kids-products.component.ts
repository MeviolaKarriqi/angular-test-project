import { Component } from '@angular/core';

@Component({
  selector: 'app-new-of-kids-products',
  template: `<app-kids-products-crud-form></app-kids-products-crud-form>`,
})
export class NewOfKidsProductsComponent  { }
