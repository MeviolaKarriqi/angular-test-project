import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCardService {

  shoppingCardItems: any[] = [];

  constructor() { }
  addProduct = (product: any) => {
    let items = this.getShoppingCardItems();
    if(items){
      items.push(product)
      localStorage.setItem('shoppingCard', JSON.stringify(items));
    }else{
      this.shoppingCardItems.push(product);
      localStorage.setItem('shoppingCard', JSON.stringify(this.shoppingCardItems));
    }
  }
   getShoppingCardItems = () =>{
     //@ts-ignore
    return JSON.parse(localStorage.getItem('shoppingCard'));
   }

   getCardLength = () =>{
     let items = this.getShoppingCardItems();
     return items? this.getShoppingCardItems().length: 0
   }
   getTotal = () =>{
     let items = this.getShoppingCardItems();
     //@ts-ignore
     return items?.reduce((acc, item) => acc+ item.price, 0);
   }

   //@ts-ignore
   removerItem = (product)=>{
     let items = this.getShoppingCardItems();
     //@ts-ignore
     const index = items.findIndex(item => item.id === product.id)
     if(index >=0){
       items.splice(index, 1);
       return localStorage.setItem('shoppingCard', JSON.stringify(items));
     }
   }
}
