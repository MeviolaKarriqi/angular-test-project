import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Observable, of} from 'rxjs';
import { switchMap} from 'rxjs/operators';
import { User} from '../models/admins.model';
import {AngularFireAuth} from "@angular/fire/auth";
import {RoleValidator} from "../helpers/roleValidator";

@Injectable({
  providedIn: 'root'
})
export class AdminService extends RoleValidator {
  public admin$ :Observable<User>;
  constructor(private afs: AngularFirestore,
              private afAuth: AngularFireAuth) {
    super();
    //@ts-ignore
    this.admin$ = this.afAuth.authState.pipe(
      switchMap( (admin) => {
        if (admin){
          return this.afs.doc<User>(`admins/${admin.uid}`).valueChanges();
        }
        return of(null);
      })
    )
  }

  // @ts-ignore
  async adminLogin(email: string, password: string): Promise<User>{
    try {
      // @ts-ignore
      const {admin} = await this.afAuth.signInWithEmailAndPassword(
        email,
        password
      );
      await this.updateAdminData(admin);
      return admin;
    }catch (e) {
      console.log(e);
    }
  }

  private updateAdminData(admin: User){
    const adminRef: AngularFirestoreDocument<User> = this.afs.doc(
      `admins/${admin.uid}`
    );
    const data: User = {
      uid: admin.uid,
      email: admin.email,
      password: admin.password,
      role: "Admin"
    };
    return adminRef.set(data, {merge: true});
  }
}

