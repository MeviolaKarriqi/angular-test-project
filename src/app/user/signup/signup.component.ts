import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth'
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  //@ts-ignore
  signupForm: FormGroup;
  //@ts-ignore
  firebaseErrorMessage: string;
  constructor(private authService: AuthService, private router: Router, private afAuth: AngularFireAuth) {
      this.firebaseErrorMessage ='';
   }

  ngOnInit(): void {
    this.signupForm = new FormGroup({
      'fullName': new FormControl('', Validators.required),
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required)
    });
  }

  signup(){
    if(this.signupForm.invalid)
    return;

    this.authService.signupUser(this.signupForm.value).then((result) =>{
      if(result==null){
        this.router.navigate(['/']);
      }else if(result.isValid == false)
      this.firebaseErrorMessage = result.message;
    }).catch(() =>{

    });
  }
}
