import { Component } from '@angular/core';

@Component({
  selector: 'app-edit-products',
  template: `<app-products-crud-form></app-products-crud-form>`
})
export class EditProductsComponent { }
