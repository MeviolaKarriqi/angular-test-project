'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">angular-test-project documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AllProductsModule.html" data-type="entity-link" >AllProductsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AllProductsModule-d7af90484cba97a821d82452bcbd4df0"' : 'data-target="#xs-components-links-module-AllProductsModule-d7af90484cba97a821d82452bcbd4df0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AllProductsModule-d7af90484cba97a821d82452bcbd4df0"' :
                                            'id="xs-components-links-module-AllProductsModule-d7af90484cba97a821d82452bcbd4df0"' }>
                                            <li class="link">
                                                <a href="components/KidsProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >KidsProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MenProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MenProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductsSliderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductsSliderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' : 'data-target="#xs-components-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' :
                                            'id="xs-components-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' : 'data-target="#xs-injectables-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' :
                                        'id="xs-injectables-links-module-AppModule-3c8434c072abc60d5fa49424108b3cff"' }>
                                        <li class="link">
                                            <a href="injectables/AdminService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdminService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ProductDataService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductDataService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CheckoutProcessModule.html" data-type="entity-link" >CheckoutProcessModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CheckoutProcessModule-463388b38a8e6adb8232258c3f8a40ff"' : 'data-target="#xs-components-links-module-CheckoutProcessModule-463388b38a8e6adb8232258c3f8a40ff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CheckoutProcessModule-463388b38a8e6adb8232258c3f8a40ff"' :
                                            'id="xs-components-links-module-CheckoutProcessModule-463388b38a8e6adb8232258c3f8a40ff"' }>
                                            <li class="link">
                                                <a href="components/CheckoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckoutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckoutProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckoutProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CheckoutSubtotalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CheckoutSubtotalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PaypalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaypalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CrudModule.html" data-type="entity-link" >CrudModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CrudModule-b224ad0643cce8348db7cf3ae778fa6a"' : 'data-target="#xs-components-links-module-CrudModule-b224ad0643cce8348db7cf3ae778fa6a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CrudModule-b224ad0643cce8348db7cf3ae778fa6a"' :
                                            'id="xs-components-links-module-CrudModule-b224ad0643cce8348db7cf3ae778fa6a"' }>
                                            <li class="link">
                                                <a href="components/DetailsOfKidsProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetailsOfKidsProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DetailsOfManProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetailsOfManProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DetailsOfProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DetailsOfProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditKidsProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditKidsProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditMaleProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditMaleProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/KidsProductsCrudFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >KidsProductsCrudFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListOfKidsProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ListOfKidsProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListOfMaleProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ListOfMaleProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ListOfProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ListOfProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MaleProductsCrudFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MaleProductsCrudFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewOfKidsProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NewOfKidsProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewOfMaleProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NewOfMaleProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewOfProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NewOfProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProductsCrudFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProductsCrudFormComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomePageModule.html" data-type="entity-link" >HomePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomePageModule-6bd31d0973513812457d3c9f80ebe4ab"' : 'data-target="#xs-components-links-module-HomePageModule-6bd31d0973513812457d3c9f80ebe4ab"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomePageModule-6bd31d0973513812457d3c9f80ebe4ab"' :
                                            'id="xs-components-links-module-HomePageModule-6bd31d0973513812457d3c9f80ebe4ab"' }>
                                            <li class="link">
                                                <a href="components/BannerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BannerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link" >MaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserModule-3074785861ed7de64f4ffe7de238fb72"' : 'data-target="#xs-components-links-module-UserModule-3074785861ed7de64f4ffe7de238fb72"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserModule-3074785861ed7de64f4ffe7de238fb72"' :
                                            'id="xs-components-links-module-UserModule-3074785861ed7de64f4ffe7de238fb72"' }>
                                            <li class="link">
                                                <a href="components/AdminComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdminComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ForgotPasswordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ForgotPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SignupComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#components-links"' :
                            'data-target="#xs-components-links"' }>
                            <span class="icon ion-md-cog"></span>
                            <span>Components</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="components-links"' : 'id="xs-components-links"' }>
                            <li class="link">
                                <a href="components/ProductsFormComponent.html" data-type="entity-link" >ProductsFormComponent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/RoleValidator.html" data-type="entity-link" >RoleValidator</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AdminService.html" data-type="entity-link" >AdminService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FirebaseService.html" data-type="entity-link" >FirebaseService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/KidsProductsCrudService.html" data-type="entity-link" >KidsProductsCrudService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MenProductsCrudService.html" data-type="entity-link" >MenProductsCrudService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProductCrudService.html" data-type="entity-link" >ProductCrudService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProductDataService.html" data-type="entity-link" >ProductDataService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ShoppingCardService.html" data-type="entity-link" >ShoppingCardService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthAdminGuard.html" data-type="entity-link" >AuthAdminGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ProductModel.html" data-type="entity-link" >ProductModel</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});