import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProductsComponent} from "./products/products.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "../app-routing.module";
import { ProductsSliderComponent } from './products-slider/products-slider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MenProductsComponent } from './men-products/men-products.component';
import { KidsProductsComponent } from './kids-products/kids-products.component';
import {MaterialModule} from "../material/material.module";



@NgModule({
  declarations: [
    ProductsComponent,
    ProductsSliderComponent,
    MenProductsComponent,
    KidsProductsComponent,

  ],
  imports: [
    CommonModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    RouterModule,
    AppRoutingModule,
    NgbModule,
    MaterialModule
  ],
  exports: [
    ProductsComponent,
    ProductsSliderComponent
  ]
})
export class AllProductsModule { }
